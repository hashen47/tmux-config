# Tmux Configuration 


## Setup
- first install the tmux plugin manager 
    ```
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
    ```

- then go to your home folder 
    ```
    cd
    ```

- then create a .tmux.conf file (if not exists)
    ```
    touch .tmux.conf
    ```

- then copy the content of the repo's .tmux.conf file and add them to your local .tmux.conf file


- then go to tmux
    ```
    tmux
    ```

- then source the .tmux.conf file
    ```
    tmux source .tmux.conf
    ```

- then source the .tmux.conf file
    ```
    tmux source .tmux.conf
    ```

- then press **Ctrl + a I** to install packages

- then you good to go :blush:

